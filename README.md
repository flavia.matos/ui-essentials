# Feelings

This project acts as a diary. You can add new entries and edit/delete old ones.

- API mock: json-server;
- CSS framework: Bootstrap;
- Application bundler: Parcel;

## How to run

1. Install dependencies with`yarn`
2. Run API mock with `yarn mock-api`
3. Run application with `yarn dev`
4. Go to `localhost:8080`
