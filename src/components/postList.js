import Post from "./post";
import Modal from "./modal";
import { getPosts } from "../controllers/post";

const PostList = async() => {
    const posts = await getPosts();
    let listItems = "";
    posts.forEach(({text, date, id}) => {
        listItems += Post(text, date, id);
    });
    const template = `
        <ul>${listItems}</ul>
        ${ Modal() }
    `;
    return template;
}

export default PostList;