import { addPost } from "../controllers/post";
import Alert from "./alert";
import app from "../app";

const PostInput = () => {
    return `<div class="input-group mb-5">
        <span class="input-group-text bg-primary border-primary">How are you feeling?</span>
        <textarea class="form-control border-primary" aria-label="How are you feeling"></textarea>
        <button class="btn btn-primary" type="button" id="submit">Send</button>
    </div>`;
}

export function handleAddPost() {
    document.getElementById("submit").addEventListener("click", addNewPost);
}

async function addNewPost(e) {
    e.preventDefault();
    const text = e.target.parentElement.childNodes[3].value;
    if (!text) {
        Alert("warning", "you can't post empty feelings :(");
        return;
    }
    const currentDate = new Date();
    const date = `${currentDate.getMonth() + 1}/${currentDate.getDate()}/${currentDate.getFullYear()}`;
    const status = addPost(text, date);
    status ? Alert("success", "post added! :)") : Alert("danger", "something went wrong.");
    app();
}

export default PostInput;
