const Alert = (classname, text) => {
    const div = document.createElement("div");
    div.className = `alert alert-${classname} alert-dismissible fade show`;
    div.role = "alert";
    div.innerText = text;
    const button = document.createElement("button");
    button.className = "btn-close";
    button.dataset["bsDismiss"] = "alert";
    div.append(button);
    const postInput = document.getElementById("post-input");
    document.querySelector("main").insertBefore(div, postInput);
}

export default Alert;