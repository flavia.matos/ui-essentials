import app from "../app";
import { deletePost, getPost, patchPost } from "../controllers/post";
import Alert from "./alert";

const Post = (text, date, id) => {
    return `<div class="post my-3 py-3 px-4" id="${id}">
        <div>
            <p class="date"> ${date}: </p>
            <p class="lead"> ${text} </p>
        </div>
        <div class="buttons">
            <button id="btn-remove-${id}"  class="btn btn-outline-danger mx-2 delete-btn">
                Delete
            </button>
            <button id="btn-edit-${id}" type="button" class="btn btn-primary edit-btn" data-bs-toggle="modal" data-bs-target="#modal">
                Edit
            </button>
        </div>
    </div>`;
}

export function handleRemovePost() {
    [...document.getElementsByClassName("btn-outline-danger")].forEach((btn) => btn.addEventListener("click", removePost));
}

export function handleEditPost() {
    [...document.getElementsByClassName("edit-btn")].forEach((btn) => btn.addEventListener("click", fillModal));
    document.getElementById("save-post").addEventListener("click", savePost);
}

async function fillModal(e) {
    e.preventDefault();
    const id = e.target.parentElement.parentElement.id;
    const textarea = document.getElementById("edit-post");
    const post = await getPost(id);
    textarea.value = post.text;
    textarea.dataset.postid = id;
}

async function savePost(e) {
    e.preventDefault();
    const textarea = document.getElementById("edit-post");
    const id = textarea.dataset.postid;
    const text = textarea.value;
    const status = await patchPost(id, text);
    status ? Alert("success", "Post edited! :)") : Alert("danger", "Something went wrong.");
    app();
}

async function removePost(e) {
    e.preventDefault();
    const id = e.target.parentElement.parentElement.id;
    const status = await deletePost(id);
    if (status) {
        e.target.parentElement.parentElement.remove();
        Alert("success", "Post deleted! :)");
    } else {
        Alert("danger", "Something went wrong.")
    }
}

export default Post;
