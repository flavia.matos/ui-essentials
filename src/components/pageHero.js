import image from "../../assets/love.svg";
const PageHero = () => {
    return `<div class="px-4 py-5 my-5 text-center">
        <img class="d-block mx-auto mb-4 logo" src="${image}" alt="" width="72" height="57">
        <h1 class="display-5 fw-bold">Welcome to Feelings</h1>
        <div class="col-lg-6 mx-auto">
            <p class="lead mb-4">Tell the world how you're feeling anonymously.</p>
        </div>
    </div>`;
}

export default PageHero;