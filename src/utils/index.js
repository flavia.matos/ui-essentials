import { handleAddPost } from "../components/postInput";
import { handleRemovePost, handleEditPost } from "../components/post";

function addEventHandlers() {
    handleAddPost();
    handleRemovePost();
    handleEditPost(); 
}

export default addEventHandlers;