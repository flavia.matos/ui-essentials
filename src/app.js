import PostInput from "./components/postInput";
import PostList from "./components/postList";
import PageHero from "./components/pageHero";
import addEventHandlers from "./utils";
import "./style/index.scss";

const app = async () => {
    document.getElementById("page-hero").innerHTML = PageHero();
    document.getElementById("post-input").innerHTML = PostInput();
    document.getElementById("post-list").innerHTML = await PostList();
    addEventHandlers();
}

app();

export default app;

