import axios from "axios";

export async function getPost(id) {
    try {
        const post = await axios.get(`http://localhost:3000/posts/${id}`);
        return post.data;
    }
    catch(err) {
        console.log(err);
    }
}

export async function getPosts() {
    try {
        const posts = await axios.get(`http://localhost:3000/posts?_sort=id&_order=desc`);
        return posts.data;
    }
    catch(err) {
        console.log(err);
    }
}

export async function addPost(text, date) {
    try {
        const res = await axios.post("http://localhost:3000/posts", {text, date});
        return res.status === 200 ? true : false;
    } catch(err) {
        console.log(err);
    }
}

export async function patchPost(id, text) {
    try {
        const res = await axios.patch(`http://localhost:3000/posts/${id}`, {text});
        return res.status === 200 ? true : false;
    }
    catch(err) {
        console.log(err);
    }
}

export async function deletePost(id) {
    try {
        const res = await axios.delete(`http://localhost:3000/posts/${id}`);
        return res.status === 200 ? true : false;
    } catch(err) {
        console.log(err);
    }
}
